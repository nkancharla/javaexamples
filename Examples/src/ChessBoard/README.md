Here is the problem that needs to be solved.

 
Problem : Move the chess piece "KNIGHT" from any location on a
          "3 x 3" Chess Board and make it go to the far right
          hand bottom corner^. Chess Board in the problem is
          not the usual Chess Board of 8 x 8.
 
          KNIGHT starting position may be any position on board
          Program should exit when knight moves to 3 x 3 corner.
 
          Here is how my Chess board looks.
              1       2       3
          -------------------------
          |       |       |       |
        1 |       |       |       |
          |       |       |       |
          -------------------------
          |       |       |       |
        2 |       |       |       |
          |       |       |       |
          -------------------------
          |       |       |       |
        3 |       |       |   X   | <<<<------- KNIGHT should reach
          |       |       |       |             this square.
          -------------------------
 
Remember: KNIGHT moves in specific way such as 2 steps in
             one direction and 1 step left/right.
          If the KNIGHT starts are position (2,2) then it cannot move further and
       you have to throw exception with some error message. 
              We are not interested in any UI programming. 
 
Run command: "java <some class> x y", where x is x-coordinate
             and y is y-coordinate(starting position of Knight)
             on the chess board. For this problem x & y could be
             1(min) and 3(max) values and any value(2) in between. 
                          
 
Deliver : 1) Write java code for above problem.
          2) Java code should compile and run.
          3) I will be interested in "Object Oriented Design"
              thought process.
          4) Your coding style
          5) Javadoc
          6) Makefile
          7) Your test results
 
Concentrate class design, interface design and error handling, how it will work for a 8 X 8 board, whether the knight picks the shortest path to reach and how you tested this.