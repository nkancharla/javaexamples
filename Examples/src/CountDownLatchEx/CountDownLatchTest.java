package CountDownLatchEx;

import java.util.concurrent.CountDownLatch;

public class CountDownLatchTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CountDownLatch latch = new CountDownLatch(3);
		
		for(int i=0; i<3; i++)
		{
			Thread t = new Thread(new WorkerThread(latch, i));
			t.start();
		}
		
		try{
			latch.await();
		}catch(InterruptedException iex)
		{
			
		}
		
		
		System.out.println("Done with latch processing.");


	}

}
