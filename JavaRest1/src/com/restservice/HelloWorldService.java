package com.restservice;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/hello")
public class HelloWorldService {
	
	//http://localhost:8080/JavaRest1/hello/{param}
	@GET
	@Path("/{param}")
	public Response getMsg(@PathParam("param") String msg) {
		String output = "Jersey say : "+ msg;
		
		return Response.status(200).entity(output).build();
		
	}
	
	

}
