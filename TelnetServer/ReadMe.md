This Java applications is to support "telnet" like connections.

Java TelnetServer is implemented in Simple Java. It supports multiple concurrent connections. 

only basic commands are implemented 
Unix   
       pwd,
       ls, 
       ls -la,
       rm  <dir>/<File>
       rm -rf  <dir>
       cd
       mkdir
       
WindowsOS
          pwd
          DIR
          del
          cd
          mkdir

MacOS, SolarisOS implementation is not there but supported files are careated.


How to Run this applications:
Step 1:  Java Telnet Server

       start main class  "TelnetServerThreadPooled.java"     (by default i started on port 9000)
       
Step 2:

       open command line interface (CLI) to connect to telnet server
       bash$   telnet localhost 9000           
       
Step 3:

      start running commands like pwd, ls, ls -la, mkdir, cd ..
      
            
       
       
Implementation Details:
I used fixed thread pool executor to support multiple concurrent connections.
I have used Java 7 NIO for all OS, file related operations.
This is implemented with out using Runtime Executors/ProcessBuilders.        
       
       
        


          

       