package com.telnet.core.osfactory;

import com.telnet.core.interfaces.OSType;
import com.telnet.core.util.TelnetUtil;
import com.telnet.core.util.UnixUtil;

import java.io.IOException;

public class UnixOS implements OSType {

	
	public String getPresentWorkingDirectory() {
		return null;

	}

    /**  logic for ls command
     *
     * @param lsParams
     * @param path
     * @return
     */
	public String listCurrentDirecotryDetails(String lsParams, String path) {
        try{
            return UnixUtil.executeUnixLSCommand(lsParams, path);
        }catch(IOException iex) {
            iex.printStackTrace();
            return "Error: Failed to get the list";
        }
    }

    /** cmd command
     *
     * @param cdInput
     * @param path
     * @return
     */
	public String executeCDCommand(String cdInput, String path) {
		return TelnetUtil.executeCDCommand(cdInput, path);
	}

    /** MKDIR command
     *
     * @param mkdirInput
     * @param path
     * @return
     */
	public String executeMKDIRCommand(String mkdirInput, String path) {
		return TelnetUtil.executeMKDIRCommand(mkdirInput, path);
	}
	
    public String removeFileOrDirectory(String params, boolean forceRemove, String currentPath) {
        return UnixUtil.removeFileorDirecotry(params, forceRemove, currentPath);
    }

}
