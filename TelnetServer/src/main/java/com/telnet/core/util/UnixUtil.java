package com.telnet.core.util;

import java.nio.file.*;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermissions;
import java.nio.file.attribute.FileTime;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: nareshk
 * Date: Jan 2, 2013
 * Time: 2:11:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class UnixUtil {

    public static final String RM_FILE_NOT_FOUND = "rm: cannot remove ";
    public static final String RM_ERROR_IS_A_DIRECTORY = " :Is a Directory.";
    public static final String RM_FILE_NOT_FOUND_PART2 = ": No such file or directory";
    

     /** This utility method is mainly for UNIX LS command
     *
     * @param params  are like -a   -h   for ex:  ls -la
     * @param path
     * @return
     * @throws IOException
     */
    public static String executeUnixLSCommand(String params, String path) throws IOException{

          if(params == null || params == "") {  //this will be simple "ls" command
                return executePlainLS(path);
          }else if(params.indexOf('l') >=0) {
              List<FileAttributes> list =  executeLS_L(path);
              String outputlist = "";
              for(FileAttributes fat: list) {
                   outputlist += fat.formate() + "\n";
              }

              return outputlist;
          }

        return null;

    }

    /**  This is to execute LS command with parameters
     *
     * @param strPath
     * @return
     * @throws IOException
     */
    public static List<FileAttributes> executeLS_L(String strPath) throws IOException {
        List<FileAttributes> result = new ArrayList<FileAttributes>();

        FileSystem fs = FileSystems.getDefault();
        Path p = fs.getPath(strPath);

        DirectoryStream<Path> ds = Files.newDirectoryStream(p);

        for(Path path : ds) {
            FileAttributes fts = new FileAttributes();
            PosixFileAttributes attr = Files.readAttributes(path, PosixFileAttributes.class);

            Integer noOfLinks = (Integer) Files.getAttribute(path, "unix:nlink");
            fts.setNoOfLinks(String.valueOf(noOfLinks));
            fts.setGroup(attr.group().getName());
            fts.setOwner(attr.owner().getName());
            fts.setPermissions(PosixFilePermissions.toString(attr.permissions()));
            fts.setSize(String.valueOf(attr.size()));
            FileTime filetime = attr.lastModifiedTime();
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(filetime.toMillis());
            Date dt = calendar.getTime();
            String lmformat = new SimpleDateFormat("MMM d yyyy HH:mm").format(dt);
            fts.setLastModified(lmformat);
            fts.setName(path.getFileName().toString());
            result.add(fts);
        }


        return result;
    }

    /** Method to execute Plain ls
     *
     * @param path
     * @return
     * @throws IOException
     */
    public static String executePlainLS(String path) throws IOException {

        FileSystem fs = FileSystems.getDefault();
        Path ph = fs.getPath(path);
        DirectoryStream<Path> ds = Files.newDirectoryStream(ph);

        String allFiles = "";

        for(Path p : ds) {

           allFiles = allFiles + p.getFileName() + "\n";


        }

        return allFiles;
    }

    /** This method is called when there is single parameter after "rm"
     *
     * @param filePath
     * @param Current_Directory
     * @return
     */
    public static synchronized String removeFileorDirecotry(String filePath, boolean forceRemove, String Current_Directory) {
        if(filePath != null && filePath.equals("*")) {
            if(forceRemove) {
                TelnetUtil.deletingDirecotryRecursively(Paths.get(Current_Directory));
                return TelnetUtil.SUCCESSFULLY_DELETED;
            }else {
                List<String> errList = TelnetUtil.deleteFilesOnly(Paths.get(Current_Directory));
                String err = "";
                for(String str: errList) {
                    err = err + str + "\n";
                }
                return err;
            }

        }else {
            String fullpath = Current_Directory + OSBasedOPsUtil.getSlashBasedOnOSType() + filePath;

            //stop process if the path is not a valid one
            if(!(TelnetUtil.validateOnlyPath(fullpath))) {
                return TelnetUtil.getRMErrorBasedOnOS(RM_FILE_NOT_FOUND +"'" +filePath+"'" + RM_FILE_NOT_FOUND_PART2); 
            }

            if(forceRemove) {
                TelnetUtil.deletingDirecotryRecursively(Paths.get(fullpath));
                return TelnetUtil.SUCCESSFULLY_DELETED;
            }else if(TelnetUtil.validateIfPathExistExcludeDirectory(fullpath)) {
                try{
                    Path filepath = Paths.get(fullpath);
                    if(filepath.toFile().isFile()) {
                        Files.delete(filepath);
                        return TelnetUtil.SUCCESSFULLY_DELETED;
                    }else {
                        TelnetUtil.deletingDirecotryRecursively(filepath);
                        return TelnetUtil.SUCCESSFULLY_DELETED;
                    }
                }catch(IOException iex) {
                    return "Failed to remove the file";
                }
            }else{
                return TelnetUtil.getRMErrorBasedOnOS(RM_FILE_NOT_FOUND + "'"+ filePath+"'" + RM_ERROR_IS_A_DIRECTORY);
            }
        }
    }
}
