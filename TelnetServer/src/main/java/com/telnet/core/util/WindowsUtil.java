package com.telnet.core.util;

import com.telnet.core.multithreadmodel.DirFileVisitor;

import java.io.IOException;
import java.nio.file.Paths;

/**
 * Created by IntelliJ IDEA.
 * User: nareshk
 * Date: Jan 2, 2013
 * Time: 6:08:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class WindowsUtil {
	
	 public static final String ERROR_DEL_FILE_NOT_FOUND = "Could Not Find ";
    
     /** THis method is to return current direcotry list.  ex   DIR (windows)
     *
     * @param path
     * @return
     * @throws java.io.IOException
     */
	public static String listCurrentDirectoryDetails(String path) throws IOException {
		return   DirFileVisitor.listAllFiles(path);
	}
	
	public static synchronized String deleteDirOrFile(String delpath, String current_path) {
		 String fullpath = OSBasedOPsUtil.getPathByOSType(current_path, delpath);
		 
		 if(!(TelnetUtil.validateOnlyPath(fullpath))) {
             return ERROR_DEL_FILE_NOT_FOUND + fullpath; 
         }
		 
		 TelnetUtil.deletingDirecotryRecursively(Paths.get(fullpath));
         return "Successfully deleted.";
	}
}
