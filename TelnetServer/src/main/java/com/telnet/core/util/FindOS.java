package com.telnet.core.util;


import com.telnet.core.interfaces.OSType;
import com.telnet.core.osfactory.MacOS;
import com.telnet.core.osfactory.SolarisOS;
import com.telnet.core.osfactory.UnixOS;
import com.telnet.core.osfactory.WindowsOS;

public class FindOS {

	private static String OS = System.getProperty("os.name").toLowerCase();
	
	public static OSType findOSUnderneath() {
		 
		System.out.println(OS);
 
		if (isWindows()) {
			System.out.println("This is Windows");
			return new WindowsOS();
		} else if (isMac()) {
			System.out.println("This is Mac");
			return new MacOS();
		} else if (isUnix()) {
			System.out.println("This is Unix or Linux");
			return new UnixOS();
		} else if (isSolaris()) {
			System.out.println("This is Solaris");
			return new SolarisOS();
		} else {
			System.out.println("Your OS is not support!!");
			return null;
		}
		
		
	}
	
	public static boolean isWindows() {
		 
		return (OS.indexOf("win") >= 0);
 
	}
 
	public static boolean isMac() {
 
		return (OS.indexOf("mac") >= 0);
 
	}
 
	public static boolean isUnix() {
 
		return (OS.indexOf("nix") >= 0 || OS.indexOf("nux") >= 0 || OS.indexOf("aix") > 0 );
 
	}
 
	public static boolean isSolaris() {
 
		return (OS.indexOf("sunos") >= 0);
 
	}

}
