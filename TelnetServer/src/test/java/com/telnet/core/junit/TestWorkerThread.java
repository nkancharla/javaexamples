package com.telnet.core.junit;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import com.telnet.core.multithreadmodel.WorkerThread;
import com.telnet.core.util.TelnetUtil;
import com.telnet.core.util.FindOS;
import com.telnet.core.util.UnixUtil;

import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: nareshk
 * Date: Dec 29, 2012
 * Time: 2:12:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestWorkerThread {
    final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Test
    public void testPositiveCDCommand() {

        String cmd = "cd backup/shared";
        new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(outContent));
        assertEquals(TelnetUtil.getUserHomeDirectory()+"/backup/shared" , outContent.toString());

        if(FindOS.isUnix()) {
            cmd = "/";
            new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(outContent));
            assertEquals("/" , outContent.toString());
        }
    }

   


    @Test
    public void testNegativeCDCommand() {
        System.out.println("....Test Case  .........testNegativeCDCommand");
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        String cmd = TelnetUtil.getUserHomeDirectory() + "/bla";
        new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(outContent));
        assertEquals(TelnetUtil.getUserHomeDirectory()+"/bla" , outContent.toString());
    }

    @Test
    public void testPositiveMkDirAndLS() {
        System.out.println("....Test Case 2 .........testPositiveMkDirAndLS");
        String cmd = "mkdir foo";
        new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(System.out, true));
        cmd = "pwd";
        new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(System.out, true));
        cmd = "ls -l";
        new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(System.out, true));
    }


    @Test
    public void testNegativeRMCommand(){
        System.out.println("..Test case 3..testPositiveRMCommand");

        if(FindOS.isUnix()){
            String cmd = "mkdir foo";
            new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(System.out, true));
            cmd = "rm foo";
            new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(outContent));
            String msg = UnixUtil.RM_FILE_NOT_FOUND + "foo" + UnixUtil.RM_ERROR_IS_A_DIRECTORY;
            assertEquals(msg , outContent.toString());
        }
    }

    @Test
    public void testPositiveRMCommand() {
        System.out.println("..Test case 3..testPositiveRMCommand");

        if(FindOS.isUnix()){
            String cmd = "mkdir foo";
            new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(System.out, true));
            cmd = "rm -rf foo";
            new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintStream(outContent));           
            assertEquals(TelnetUtil.SUCCESSFULLY_DELETED , outContent.toString());
        }
    }
    
    @Test
    public void testMultiInputFlow() {
        String cmd = "pwd";
      //  ParseCLICommand pcl = new ParseCLICommand("pwd", new PrintWriter(System.out, true), new ByteArrayInputStream(cmd.getBytes()));
        //pcl.parseInputCommand(null);

        for(int i =1; i < 5;i++) {
             if(i == 2) {
                 cmd = "cd backup";
             }else if(i ==3) {
                 cmd = "ls -l";
             }

          //  new WorkerThread(new ByteArrayInputStream(cmd.getBytes()), new ByteArrayOutputStream(), "Thread pool server for each client").parseInputCommand(cmd, new PrintWriter(System.out, true));
        }
    }
}
