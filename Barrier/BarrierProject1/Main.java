package Barrier.BarrierProject1;

class ParallelComputation implements Runnable {
    public void run() {
        // DO SOME COMPUTATION
	// now wait for others to finish
        try {
            Main.barrier.waitForRelease();
        }
        catch(InterruptedException e) {}
    }
}

public class Main {
    public static Barrier barrier = new Barrier(3);
    public static void main(String[] args) {
        new Thread(new ParallelComputation()).start();
        new Thread(new ParallelComputation()).start();
	// if you comment this one out, program hangs!
        new Thread(new ParallelComputation()).start();
    }
}
